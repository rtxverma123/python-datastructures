class Node:
    def __init__(self,data=None,index=None):
        self.data=data
        self.index=index
    
class LinkedList:
    def __init__(self):
        self.head = None
    
    def insert_at_beginning(self, data):
        self.head = Node(data,self.head)
    
    def insert_at_end(self,data):
        itr = self.head
        while itr.next:
            itr = itr.next
        
        itr.next = Node(data,None)
    
    def count_values(self):
        itr = self.head
        count =0
        while itr.next:
            count = count + 1
            itr = itr.next
        return count
    
    def insert_at_point(self,data,index):
        if index < 0 or index > self.count_values
        itr = self.head
        count = 0
        while itr.next:
            if count == index - 1:
                itr.next = Node(data,itr.next)
                break
            itr=itr.next
            count = count + 1
    
    def print(self):
        itr = self.head
        lst = ''
        while itr.next:
            lst = lst + str(itr.data) + '-->'
            itr = itr.next
        print(lst)
    
    if __name__ == "__main__":
        ll = LinkedList()
        ll.insert_at_beginning(11)
        ll.insert_at_beginning(21)
        ll.insert_at_beginning(19)
        ll.insert_at_point(15,1)
        ll.print()
        

            
