def balancedBracket(exp):
    stack = []

    for char in exp:
        if char == "{" or char == "[" or char == "(":
            stack.append(char)
        
        else : 
            current_char = stack.pop()
            if current_char == "(":
                if char !=")":
                    return false
            if current_char == "{":
                if char != "}":
                    return false
            
            if current_char == "[":
                if char!="]":
                    return false
if __name__== "__main__":
    exp = "[[{)}]"
    if balancedBracket(exp):
        print("Balanced")
    else:
        print("Not balanced")
    

    

