from collections import deque

class Stack:
    def __init__(self):
        self.container = deque
    
    def push(self,val):
        self.container.append(val)
    
    def pop(self):
        return self.container.remove()

    def count(self):
        return len(self.container)

    def peek(self):
        return self.container[-1]

    def print(self):
        itr = self.container
        print(itr)
    

    if __name__ == "__main__":
        s = Stack()
        s.push(13)
        s.push(56)
        s.pop()
        s.push(19)
        s.push(18)
        s.print()